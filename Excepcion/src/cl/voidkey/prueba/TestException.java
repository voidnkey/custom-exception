package cl.voidkey.prueba;

public class TestException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public TestException() { super(); }
	
	public TestException(String message) { super(message); }
	
	public TestException ( String message, Throwable causa) { super(message,causa); }
	
	public TestException(Throwable causa) { super(causa); }

}
