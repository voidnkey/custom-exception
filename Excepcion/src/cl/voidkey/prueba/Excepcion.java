package cl.voidkey.prueba;


public class Excepcion {

	public static void main(String[] args) {
	
		try {
			
			@SuppressWarnings("unused")
			PruebaExcepcion test = new PruebaExcepcion();
			
		}catch(TestException e)
		{
			System.out.println("Excepcion de prueba "+ e.getMessage());
		}catch(Exception e)
		{
			System.out.println("Exception generica "+ e.getMessage());
		}

	}

}
